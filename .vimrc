syntax on
set encoding=utf-8

set number relativenumber
set tabstop=4
set shiftwidth=4
set noexpandtab
set smartindent

set ignorecase
set smartcase

set cursorline
set linebreak

set list
set listchars=tab:→\ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨

set nobackup nowritebackup

