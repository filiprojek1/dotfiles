local set = vim.opt

set.mouse = 'a'
set.encoding = 'utf-8'
set.fileencoding = 'UTF-8'

set.number = true
set.relativenumber = true
set.showcmd = true
vim.o.syntax = true

set.ignorecase = true
set.smartcase = true

set.cursorline = true
-- bold line number at cursor
vim.cmd [[ highlight CursorLineNr cterm=bold ]]

set.linebreak = true

-- special characters
set.list = true
set.listchars = 'tab:→ ,eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨'

set.tabstop = 4
set.shiftwidth = 4
--set.expandtab = true
vim.o.expandtab = false
vim.o.smartindent = true

set.swapfile = false

