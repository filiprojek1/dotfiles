return {
	{
		'f-person/git-blame.nvim',
		config = function()
			vim.g.gitblame_date_format = '%r'
			vim.g.gitblame_message_when_not_committed = ''
			vim.g.gitblame_display_virtual_text = 0
		end
	},
	{
		'lewis6991/gitsigns.nvim',
		config = function()
			require('gitsigns').setup()
		end
	},
}

