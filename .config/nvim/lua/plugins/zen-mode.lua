return {
	'folke/zen-mode.nvim',
	config = {
		window = {
			backdrop = 1,
			width = 80,
			height = .9,
			options = {
				signcolumn = "no",
				number = false,
				relativenumber = false,
				cursorline = false,
				cursorcolumn = false,
				foldcolumn = "0",
				list = false,
			}
		},
		opts = {
			alacritty = {
				enabled = true,
				font = "13",
			},
		},
	},
	keys = {
		{ '<leader>fg', '<cmd>ZenMode<cr>', desc = 'Zen Mode' },
	},
}

