return {
	'RRethy/nvim-base16',
	'tpope/vim-commentary',

	-- completion
	'hrsh7th/nvim-cmp',
	'hrsh7th/cmp-buffer',
	'hrsh7th/cmp-path',
	'hrsh7th/cmp-cmdline',
	'saadparwaiz1/cmp_luasnip',

	-- snippets
	'L3MON4D3/LuaSnip',
	'rafamadriz/friendly-snippets',

	-- emmet
	'mattn/emmet-vim',

	-- LSP
	'neovim/nvim-lspconfig',

	-- LSP, DAP, linters and formatters
	-- 'jose-elias-alvarez/null-ls.nvim',
	{
		'dense-analysis/ale',
		config = function()
			vim.cmd [[ g:ale_sign_column_always = 1 ]]
		end
	},

	-- management for LSP, DAP, linters and formatters
	{
		'williamboman/mason.nvim',
		config = function()
			require('mason').setup({
				ui = {
					icons = {
						package_installed = "✓",
						package_pending = "➜",
						package_uninstalled = "✗"
					}
				}
			})
		end
	},
	'williamboman/mason-lspconfig.nvim',
}

