return {
	'nvim-lualine/lualine.nvim',
	dependencies = { 'kyazdani42/nvim-web-devicons', lazy = true },
	config = function()
		local git_blame = require('gitblame')
		require('lualine').setup({
			sections = {
				lualine_c = {
					{ git_blame.get_current_blame_text, cond = git_blame.is_blame_text_available }
				}
			}
		})
		vim.o.showmode = false
	end
}

